//
//  HomeViewModel.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    @Published private(set) var blockGrid: BlockGrid = BlockGrid()
    @Published private(set) var isPlaying: Bool = true
    @Published private(set) var score: Int = 0
    
    private let animator = PassthroughSubject<Block, Never>()
    private var cancellables: [AnyCancellable] = []
    private var canTapMore: Bool = true
    private var tappedBlocks = 0
    
    init() {
        animator
            .buffer(size: .max, prefetch: .byRequest, whenFull: .dropNewest)
            .flatMap(maxPublishers: .max(1)) { Just($0).delay(for: 0.1, scheduler: RunLoop.main) }
            .sink { [weak self] item in
                self?.onBlockTap(item, isDropping: true)
            }
            .store(in: &cancellables)
    }
    
    deinit {
        cancellables.forEach { $0.cancel() }
    }
    
    /// On block tap
    /// - Parameter block: block
    func blockTapped(_ block: Block) {
        guard canTapMore && !blockGrid.isBlockAlreadyFilled(block.row, block.column) else {
            return
        }
        canTapMore = false
        tappedBlocks += 1
        onBlockTap(block, isDropping: false)
    }
    
    private func onBlockTap(_ block: Block, isDropping: Bool) {
        if let nextBlock = blockGrid.onBlockTap(block.row, block.column, isDropping: isDropping) {
            animator.send(nextBlock)
        } else {
            canTapMore = true
            if tappedBlocks >= Constants.maxSelectableBlocks {
                isPlaying = false
                score = blockGrid.score()
                canTapMore = false
            }
        }
    }
    
    /// Restart the game
    func restartGame() {
        blockGrid = BlockGrid()
        isPlaying = true
        score = 0
        canTapMore = true
        tappedBlocks = 0
    }
}
