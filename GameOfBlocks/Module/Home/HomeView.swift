//
//  HomeView.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var viewModel: HomeViewModel = HomeViewModel()
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack {
                    Text("Game of Blocks")
                        .font(.title)
                        .bold()
                    Spacer()
                    if !viewModel.isPlaying {
                        HomeScoreView(score: viewModel.score) {
                            viewModel.restartGame()
                        }
                    }
                }
                .frame(width: geometry.size.width, height: geometry.size.height)
                HomeGridView(grid: viewModel.blockGrid.grid, gridDimension: geometry.size.width - 20, isPLaying: viewModel.isPlaying) { block in
                    viewModel.blockTapped(block)
                }
                .padding(.bottom, 100)
            }
        }
    }
}

struct HomeGridView: View {
    
    let grid: [[Block]]
    let gridDimension: CGFloat
    let isPLaying: Bool
    let onTap: (Block) -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            ForEach(grid.reversed(), id: \.self) { row in
                HStack(spacing: 0) {
                    ForEach(row, id: \.self) { block in
                        Text(isPLaying || block.points == 0 ? "" : "\(block.points)")
                            .frame(width: gridDimension / CGFloat(Constants.gridSize), height: gridDimension / CGFloat(Constants.gridSize))
                            .background(block.isFilled ? Color.blue : Color.white)
                            .border(Color.black, width: 1)
                            .onTapGesture {
                                onTap(block)
                            }
                    }
                }
            }
        }
        .frame(width: gridDimension, height: gridDimension)
        .border(Color.black, width: 2)
    }
}

struct HomeScoreView: View {
    
    let score: Int
    let onTap: () -> Void
    
    var body: some View {
        VStack {
            Text("Score: \(score)")
                .padding(24)
            Button("Restart") {
                onTap()
            }
            .foregroundColor(.white)
            .padding(EdgeInsets(top: 12, leading: 24, bottom: 12, trailing: 24))
            .background(Color.black)
            .cornerRadius(24)
        }
        .padding(.bottom, 32)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct HomeScoreView_Previews: PreviewProvider {
    static var previews: some View {
        HomeScoreView(score: 100) {
            
        }
    }
}
