//
//  BlockGrid.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 22/05/21.
//

import Foundation

struct BlockGrid {
    
    private(set) var grid: [[Block]]
    private var lastSelectedBlock: Block?
    
    init() {
        grid = Array(0..<Constants.gridSize).map { row in
            Array(0..<Constants.gridSize).map { Block(row: row, column: $0) }
        }
    }
    
    /// Check if a block is already filled
    /// - Parameter row: block row
    /// - Parameter column: block column
    /// - Returns: is block filled
    func isBlockAlreadyFilled(_ row: Int, _ column: Int) -> Bool {
        return grid[row][column].isFilled
    }
    
    /// Tap on a block and return next block to fill
    /// - Parameter row: block row
    /// - Parameter column: block column
    /// - Parameter isDropping: is block dropping
    /// - Returns: next block to fill
    mutating func onBlockTap(_ row: Int, _ column: Int, isDropping: Bool) -> Block? {
        guard !isBlockAlreadyFilled(row, column) else {
            return nil
        }
        if isDropping, let lastBlock = lastSelectedBlock, lastBlock.column == column {
            grid[lastBlock.row][lastBlock.column].isFilled = false
        }
        grid[row][column].isFilled = true
        lastSelectedBlock = grid[row][column]
        guard canMove(grid[row][column]) else {
            return nil
        }
        return grid[row - 1][column]
    }
    
    /// Retrieve final score
    /// - Returns: score
    func score() -> Int {
        grid.reduce([], +).compactMap { $0.points }.reduce(0, +)
    }
    
    private mutating func canMove(_ block: Block) -> Bool {
        guard block.row > 0 else {
            grid[block.row][block.column].points = Constants.pointsSingleFilledBlock
            return false
        }
        guard !grid[block.row - 1][block.column].isFilled else {
            grid[block.row][block.column].points = grid[block.row - 1][block.column].points + Constants.pointsSingleFilledBlock
            return false
        }
        // Look for bridge
        var canMove = false
        switch block.column {
        case 0:
            canMove = !grid[block.row][block.column + 1].isFilled
        case Constants.gridSize - 1:
            canMove = !grid[block.row][block.column - 1].isFilled
        default:
            canMove = !grid[block.row][block.column - 1].isFilled || !grid[block.row][block.column + 1].isFilled
        }
        if !canMove {
            grid[block.row][block.column].points = Constants.pointsSingleFilledBlock
            updateBlockUnderBridgePoints(block)
        }
        return canMove
    }
    
    private mutating func updateBlockUnderBridgePoints(_ block: Block) {
        var row = block.row - 1
        while row >= 0 && !grid[row][block.column].isFilled {
            grid[row][block.column].points = Constants.pointsBlockUnderBridge
            row -= 1
        }
    }
}
