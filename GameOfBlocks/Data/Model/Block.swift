//
//  Block.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import Foundation

struct Block: Hashable {
    
    var row: Int
    var column: Int
    var isFilled: Bool
    var points: Int
    
    init(row: Int, column: Int) {
        self.row = row
        self.column = column
        self.isFilled = false
        self.points = 0
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(row)
        hasher.combine(column)
    }
}
