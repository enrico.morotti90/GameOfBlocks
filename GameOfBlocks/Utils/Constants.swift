//
//  Constants.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import Foundation

struct Constants {
    static let gridSize = 5
    static let maxSelectableBlocks = 10
    
    static let pointsSingleFilledBlock = 5
    static let pointsBlockUnderBridge = 10
}
