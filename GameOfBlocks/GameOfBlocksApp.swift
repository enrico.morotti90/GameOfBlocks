//
//  GameOfBlocksApp.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import SwiftUI

@main
struct GameOfBlocksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
