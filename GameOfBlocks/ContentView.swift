//
//  ContentView.swift
//  GameOfBlocks
//
//  Created by Enrico Morotti on 20/05/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
